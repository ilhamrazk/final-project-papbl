package com.example.ilhamrazk.projectilham;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.ilhamrazk.projectilham.Adapter.ObatAdapter;
import com.example.ilhamrazk.projectilham.Model.Obat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ListObatActivity extends AppCompatActivity {

    ListView listView;
    List<Obat> obatList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_obat);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        listView = findViewById(R.id.list_obat);
        obatList = new ArrayList<>();
        showList();
    }

    private void showList() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://192.168.0.10/obat/listsemuaobat.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("semuaObat");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject obatObj = array.getJSONObject(i);
                                Obat p = new Obat(obatObj.getString("id"), obatObj.getString("obat"));
                                obatList.add(p);
                            }
                            ObatAdapter adapter = new ObatAdapter(obatList, getApplicationContext());
                            listView.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){

        };
        Handler.getInstance(getApplicationContext()).addToRequestQue(stringRequest);
    }
}
