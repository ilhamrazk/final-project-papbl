package com.example.ilhamrazk.projectilham.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.ilhamrazk.projectilham.Model.Obat;
import com.example.ilhamrazk.projectilham.R;

import java.util.List;

public class ObatAdapter extends ArrayAdapter<Obat> {

    private List<Obat> obatList;
    private Context mCtx;

    public ObatAdapter(List<Obat> P, Context c) {
        super(c, R.layout.listobat, P);
        this.obatList = P;
        this.mCtx = c;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.listobat,null,true);

        TextView nama = (TextView) view.findViewById(R.id.tvNamaObat);

        Obat obat = obatList.get(position);
        nama.setText(obat.getNama());

        return  view;
    }
}
