package com.example.ilhamrazk.projectilham.Model;

public class Obat {
    String Id,Nama;

    public Obat(String id, String nama) {
        Id = id;
        Nama = nama;
    }

    public String getId() {
        return Id;
    }

    public String getNama() {
        return Nama;
    }
}
