package com.example.ilhamrazk.projectilham;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.ilhamrazk.projectilham.Fragment.DatePickerFragment;
import com.example.ilhamrazk.projectilham.Fragment.TimePickerFragment;
import com.example.ilhamrazk.projectilham.Interface.DataInterface;
import com.example.ilhamrazk.projectilham.Model.DataObat;
import com.example.ilhamrazk.projectilham.Receiver.AlarmReceiver;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, DatePickerFragment.DialogDateListener, TimePickerFragment.DialogTimeListener {

    TextView tvOnceDate;
    TextView tvOnceTime;
    EditText edtOnceMessage;
    ImageButton btnOnceDate;
    ImageButton btnOnceTime;
    Button btnSetOnce, btnMoveActivity;

    private AlarmReceiver alarmReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        tvOnceDate = findViewById(R.id.tv_once_date);
        btnOnceDate = findViewById(R.id.btn_once_date);
        tvOnceTime = findViewById(R.id.tv_once_time);
        btnOnceTime = findViewById(R.id.btn_once_time);
        edtOnceMessage = findViewById(R.id.edt_once_message);
        btnSetOnce = findViewById(R.id.btn_set_once_alarm);
        btnMoveActivity = findViewById(R.id.btn_list_obat);

        btnOnceDate.setOnClickListener(this);
        btnOnceTime.setOnClickListener(this);
        btnSetOnce.setOnClickListener(this);
        btnMoveActivity.setOnClickListener(this);

        alarmReceiver = new AlarmReceiver();
    }

    final String DATE_PICKER_TAG = "DatePicker";
    final String TIME_PICKER_ONCE_TAG = "TimePickerOnce";
    final String TIME_PICKER_REPEAT_TAG = "TimePickerRepeat";

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_once_date:
                DatePickerFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.show(getSupportFragmentManager(), DATE_PICKER_TAG);
                break;
            case R.id.btn_once_time:
                TimePickerFragment timePickerFragmentOne = new TimePickerFragment();
                timePickerFragmentOne.show(getSupportFragmentManager(), TIME_PICKER_ONCE_TAG);
                break;
            case R.id.btn_set_once_alarm:
                String onceDate = tvOnceDate.getText().toString();
                String onceTime = tvOnceTime.getText().toString();
                String onceMessage = edtOnceMessage.getText().toString();

                alarmReceiver.setOneTimeAlarm(this, AlarmReceiver.TYPE_ONE_TIME,onceDate,onceTime,onceMessage);
                break;
            case R.id.btn_list_obat:
                Intent intent = new Intent(this, ListObatActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onDialogDateSet(String tag, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        tvOnceDate.setText(dateFormat.format(calendar.getTime()));
    }

    @Override
    public void onDialogTimeSet(String tag, int hourOfDay, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        switch (tag) {
            case TIME_PICKER_ONCE_TAG:
                tvOnceTime.setText(dateFormat.format(calendar.getTime()));
                break;
            default:
                break;
        }
    }

    private void loadObat(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(getResources().getString(R.string.url_json))
                .addConverterFactory(GsonConverterFactory.create()).build();

        DataInterface dataInterface = retrofit.create(DataInterface.class);

        Call<List<DataObat>> callData = dataInterface.callObat();
        callData.enqueue(new Callback<List<DataObat>>() {
            @Override
            public void onResponse(Call<List<DataObat>> call, Response<List<DataObat>> response) {
                if (response.isSuccessful()){
                    for (DataObat tempat:response.body()){
                        Log.e("LOG","Nama : "+tempat.getDrugs());
                        Log.e("LOG","Deskripsi : "+tempat.getDescription());

                    }
                }
            }

            @Override
            public void onFailure(Call<List<DataObat>> call, Throwable t) {

            }
        });
    }
}
