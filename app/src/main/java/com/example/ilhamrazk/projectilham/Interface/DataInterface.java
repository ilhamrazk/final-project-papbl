package com.example.ilhamrazk.projectilham.Interface;


import com.example.ilhamrazk.projectilham.Model.DataObat;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface DataInterface {

    @GET("drugs.json")
    Call<List<DataObat>> callObat();
}
